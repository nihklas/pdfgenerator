{
  description = "Japr: Just-Another-Pdf-Renderer, converts HTML files into PDFs";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = {
    self,
    nixpkgs,
  }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
    };
    bin = with pkgs;
      stdenv.mkDerivation {
        name = "japr";
        src = self;
        nativeBuildInputs = [
          cmake
          qt6.wrapQtAppsNoGuiHook
        ];
        buildInputs = [
          qt6.qtwebengine
          qt6.qtbase
        ];
      };
  in
    with pkgs; {
      devShells.x86_64-linux.default = mkShell {
        packages = [
          cmake
          qt6.full
          qt6.qtbase
        ];
      };

      packages.x86_64-linux.default = bin;

      docker = dockerTools.buildLayeredImage {
        name = "nihklas/japr";
        tag = "latest";
        contents = [bin];
        config.Entrypoint = ["/bin/japr" "-platform" "offscreen" "/in" "/out"];
        config.Env = ["QTWEBENGINE_DISABLE_SANDBOX=1"];
      };
    };
}
