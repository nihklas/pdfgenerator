# Japr - Just-Another-PDF-Renderer

Japr is an HTML-To-PDF-Converter. It stems from the lack of up-to-date 
Converters, that do no rely on a complete Chromium for rendering and printing 
(like [Puppeteer](https://github.com/puppeteer/puppeteer/tree/main#readme)). 
As [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf) is discontinued and
now archived, Japr was created as a new alternative for an easy-to-use Convert,
that uses the modern Browser Standards for HTML-Rendering. It uses the 
[Qt WebEngine](https://doc.qt.io/qt-6/qtwebengine-overview.html), so the 
rendering is based on the Chromium project, and therefore has the newest Browser
Standard for HTML/CSS as well as even JavaScript.

Since version 2.0.0, these images are built using Nix and only support x86_64
(amd64) architecture. If you are on a different CPU architecture, either use 
version 1.1.6, or build Japr from source.

## Running Containers

```bash
docker run --rm -v <input-folder>:/in -v <output-folder>:/out nihklas/japr
```

Converting all files in `<input-folder>` and putting the generated files into `<output-folder>`.

