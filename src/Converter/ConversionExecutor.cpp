//
// Created by Niklas Koll on 17.05.24.
//

#include "ConversionExecutor.h"

#include <QtCore/QTimer>
#include <QtWidgets/QApplication>

void ConversionExecutor::registerWorker(const ConvertedFile& file, Converter& converter, ConverterConfig config)
{

    workerFunctions.emplace([file, &converter, &config]() {
        int returnCode = converter.convert(file, config);
        if (returnCode != 0) {
            QTextStream(stderr) << "Something went wrong in the Conversion. "
                                   "Completetly shutting down."
                                << "\n";
            QApplication::exit(returnCode);
            return;
        }
    });
}

// Sequentially work through callback stack
// When one conversion is done, the signal is emitted, which starts the next
// working iteration
void ConversionExecutor::run(Converter& converter)
{
    if (workerFunctions.empty()) {
        QTimer::singleShot(0, QApplication::quit);
        return;
    }
    connect(&converter, SIGNAL(finishedConverting()), this,
        SLOT(finishedWorker()));
    work();
}

void ConversionExecutor::finishedWorker()
{
    // pop the just executed function from the stack
    workerFunctions.pop();
    if (workerFunctions.empty()) {
        // done
        QApplication::quit();
        return;
    }

    // get the next one
    work();
}

void ConversionExecutor::work()
{
    auto function = workerFunctions.top();
    // at the end of "function" the "finishedConverting()" signal is emitted
    QTimer::singleShot(0, function);
}
