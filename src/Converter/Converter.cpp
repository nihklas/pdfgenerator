//
// Created by Niklas Koll on 17.05.24.
//

#include <QtCore/QDir>
#include <QtCore/QEventLoop>
#include <QtCore/QFileInfo>

#include "Converter.h"

/// returns 0 on success, any other value is an errorcode
/// 1 => argument error;
/// 2 => error in conversion process
int Converter::convert(const ConvertedFile& file, const ConverterConfig& config)
{
    auto input = file.html_file;
    auto output = file.pdf_file;
    auto inputFile = QFileInfo(input);
    auto outputFile = QFileInfo(output);
    QEventLoop loop;

    if (!inputFile.exists()) {
        QTextStream(stderr) << "Input File " << input << " does not exist." << "\n";
        return 1;
    }

    if (!inputFile.suffix().endsWith("html")) {
        QTextStream(stderr) << "Input File " << input
                            << " cannot be converted. Only .html Files are allowed."
                            << "\n";
        return 1;
    }

    if (outputFile.suffix().isEmpty()) {
        outputFile.setFile(QDir(outputFile.absoluteFilePath()),
            inputFile.baseName() + ".pdf");
    }

    if (!outputFile.dir().exists()) {
        QTextStream(stderr) << "Target Directory " << outputFile.dir().path()
                            << " does not exist." << "\n";
        return 1;
    }

    auto printer = std::make_unique<QPrinter>();
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setResolution(config.dpi);
    printer->setPageLayout(Converter::getLayoutFromConfig(config));
    printer->setCreator("japr");
    printer->setOutputFileName(outputFile.absoluteFilePath());

    QWebEngineView view;
    view.setFixedSize(config.page_size.sizePoints());
    view.show();

    connect(&view, &QWebEngineView::loadFinished, [&](bool ok) {
        if (!ok) {
            QTextStream(stderr) << "Failed to load file " << input << "\n";
            loop.exit(2);
            return;
        }

        view.print(printer.get());
    });

    connect(&view, &QWebEngineView::printFinished, [&](bool ok) {
        if (!ok) {
            QTextStream(stderr) << "Failed to print file " << output << "\n";
            loop.exit(2);
            return;
        }
        QTextStream(stdout) << "PDF File generated: "
                            << QDir::current().relativeFilePath(
                                   outputFile.absoluteFilePath())
                            << "\n";
        loop.quit();
        emit finishedConverting();
    });

    view.load(QUrl::fromLocalFile(inputFile.absoluteFilePath()));

    return loop.exec();
}

QPageLayout Converter::getLayoutFromConfig(const ConverterConfig& config)
{
    return { config.page_size,
        config.orientation_portrait ? QPageLayout::Portrait
                                    : QPageLayout::Landscape,
        QMarginsF(config.margin_left, config.margin_top, config.margin_right,
            config.margin_bottom) };
}
