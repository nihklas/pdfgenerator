//
// Created by Niklas Koll on 17.05.24.
//

#include "Japr.h"
#include "ConversionExecutor.h"
#include "Converter.h"
#include "HeaderFooterHandler.h"

#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include <QtCore/QFileInfo>
#include <QtCore/qglobal.h>
#include <QtWidgets/QApplication>

int Japr::run()
{
    if (!checkFiles()) {
        return 1;
    }

    readAllFiles();

    Converter converter;

    ConverterConfig headerRenderConfig {
        .orientation_portrait = options.orientation == "Portrait",
        .page_size = options.page_size == "A4" ? QPageSize::A4 : QPageSize::Letter,
        .margin_top = 0,
        .margin_bottom = 0,
        .margin_right = 0,
        .margin_left = 0,
        .dpi = options.dpi.toInt()
    };

    HeaderFooterHandler hfHandler(&converter, headerRenderConfig, options.header,
        options.footer, tempFilePrefix);
    int hfCalcRetCode = hfHandler.calculateHeaderFooterHeight();
    if (hfCalcRetCode != 0) {
        QTextStream(stderr)
            << "Something went wrong while calculating header and footer height."
            << "\n";
        return hfCalcRetCode;
    }

    ConverterConfig config {
        .orientation_portrait = options.orientation == "Portrait",
        .page_size = options.page_size == "A4" ? QPageSize::A4 : QPageSize::Letter,
        .margin_top = options.margin_top.toDouble() + options.header_spacing.toDouble() + hfHandler.getHeaderHeight(),
        .margin_bottom = options.margin_bottom.toDouble() + options.footer_spacing.toDouble() + hfHandler.getFooterHeight(),
        .margin_right = options.margin_right.toDouble(),
        .margin_left = options.margin_left.toDouble(),
        .dpi = options.dpi.toInt()
    };

    ConversionExecutor executor;

    for (const auto& file : files) {
        executor.registerWorker(file, converter, config);
    }

    executor.run(converter);

    int returnCode = QApplication::exec();
    // removed finished Signal from worker
    // TODO: maybe put it inside the application 'aboutToQuit' Signal, inside executor code
    QObject::disconnect(&converter, SIGNAL(finishedConverting()), &executor,
        SLOT(finishedWorker()));

    if (returnCode != 0) {
        QTextStream(stderr) << "Something went wrong. Exit Code: " << returnCode
                            << "\n";
        return returnCode;
    }

    if (!options.header.isEmpty() || !options.footer.isEmpty()) {
        for (const auto& file : files) {
            int result = hfHandler.printHeaderOnFile(file.pdf_file);
            if (result != 0) {
                return result;
            }
        }
    }

    hfHandler.cleanUp();

    return 0;
}

bool Japr::checkFiles() const
{
    auto input = arguments.input;
    auto output = arguments.output;
    auto inputFile = QFileInfo(input);
    auto outputFile = QFileInfo(output);

    if (!inputFile.exists()) {
        QTextStream(stderr) << "Input File " << input << " does not exist" << "\n";
        return false;
    }

    if (inputFile.isDir() && !(outputFile.suffix().isEmpty() || outputFile.isDir())) {
        QTextStream(stderr)
            << "If Input is a directory, output also has to be a directory" << "\n";
        return false;
    }

    if (outputFile.suffix().isEmpty() || outputFile.isDir()) {
        auto outputDir = QDir(outputFile.absoluteFilePath());
        if (!outputDir.exists()) {
            QTextStream(stderr) << "Output directory " << output << " does not exist"
                                << "\n";
            return false;
        }
    }

    return true;
}

void Japr::readAllFiles()
{
    auto input = arguments.input;
    auto output = arguments.output;
    auto inputFile = QFileInfo(input);
    auto outputPath = QFileInfo(output);

    if (inputFile.isDir()) {
        auto outputDir = QDir(outputPath.absoluteFilePath());
        QDirIterator iter(inputFile.absoluteFilePath());
        while (iter.hasNext()) {
            QString next = iter.next();
            auto nextFile = QFileInfo(next);
            if (nextFile.isDir()) {
                continue;
            }

            if (nextFile.suffix().endsWith("html")) {
                files.push_back({ .html_file = nextFile.absoluteFilePath(),
                    .pdf_file = outputDir.absoluteFilePath(
                        nextFile.baseName() + ".pdf") });
            }
        }
    } else {
        if (outputPath.suffix().isEmpty()) {
            auto outputDir = QDir(outputPath.absoluteFilePath());
            files.push_back({ .html_file = inputFile.absoluteFilePath(),
                .pdf_file = outputDir.absoluteFilePath(
                    inputFile.baseName() + ".pdf") });
        } else {
            files.push_back({ .html_file = inputFile.absoluteFilePath(),
                .pdf_file = outputPath.absoluteFilePath() });
        }
    }
}
