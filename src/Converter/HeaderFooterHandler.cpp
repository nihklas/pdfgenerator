//
// Created by Niklas Koll on 18.05.24.
//

#include <QtCore/QDir>
#include <QtCore/QEventLoop>
#include <QtCore/QFileInfo>
#include <QtGui/QPainter>
#include <QtPdf/QPdfDocument>
#include <queue>

#include "HeaderFooterHandler.h"

int HeaderFooterHandler::calculateHeaderFooterHeight()
{
    int retCode = calculateHeaderHeight();
    if (retCode != 0) {
        QTextStream(stderr)
            << "Something went wrong while calculating the header-height." << "\n";
        return retCode;
    }

    retCode = calculateFooterHeight();
    if (retCode != 0) {
        QTextStream(stderr)
            << "Something went wrong while calculating the footer-height." << "\n";
        return retCode;
    }

    return 0;
}

qreal HeaderFooterHandler::getHeaderHeight() const { return headerHeight; }

qreal HeaderFooterHandler::getFooterHeight() const { return footerHeight; }

int HeaderFooterHandler::printHeaderOnFile(const QString& pdfFile) const
{

    bool printHeader = !header.html_file.isEmpty();
    bool printFooter = !footer.html_file.isEmpty();

    if (printHeader && !QFileInfo(header.pdf_file).exists()) {
        int converterResult = converter->convert(header, config);
        if (converterResult != 0) {
            QTextStream(stderr) << "Could not render header" << "\n";
            return converterResult;
        }
    }
    if (printFooter && !QFileInfo(footer.pdf_file).exists()) {
        int converterResult = converter->convert(footer, config);
        if (converterResult != 0) {
            QTextStream(stderr) << "Could not render footer" << "\n";
            return converterResult;
        }
    }

    QTextStream(stdout) << "Printing header and/or footer on file " << pdfFile << "\n";

    QPdfDocument pdfDoc;
    pdfDoc.load(pdfFile);

    QImage headerImg;
    QImage footerImg;
    if (printHeader) {
        QPdfDocument headerDoc;
        headerDoc.load(header.pdf_file);
        headerImg = headerDoc.render(0, config.page_size.sizePixels(config.dpi));
    }
    if (printFooter) {
        QPdfDocument footerDoc;
        footerDoc.load(footer.pdf_file);
        footerImg = footerDoc.render(0, config.page_size.sizePixels(config.dpi));
    }

    std::queue<QImage> pages;
    for (int i = 0; i < pdfDoc.pageCount(); i++) {
        pages.push(pdfDoc.render(i, config.page_size.sizePixels(config.dpi)));
    }

    QPainter painter;
    QPrinter printer;
    QPageLayout layout(config.page_size,
        config.orientation_portrait ? QPageLayout::Portrait
                                    : QPageLayout::Landscape,
        QMarginsF(0, 0, 0, 0));

    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setResolution(config.dpi);
    printer.setCreator("japr");
    printer.setOutputFileName(pdfFile);
    printer.setPageLayout(layout);

    QRectF paperRect = printer.paperRect(QPrinter::DevicePixel);

    auto footerHeightCalc = (footerHeight * config.dpi) / 72;
    auto headerHeightCalc = (headerHeight * config.dpi) / 72;

    QRectF footerSrc(0, 0, paperRect.width(), footerHeightCalc);
    QRectF footerRect(0, paperRect.height() - footerHeightCalc, paperRect.width(), footerHeightCalc);
    QRectF headerRect(0, 0, paperRect.width(), headerHeightCalc);
    QRectF contentRect(0, headerHeightCalc, paperRect.width(), paperRect.height() - headerHeightCalc - footerHeightCalc);

    painter.begin(&printer);


    for (; !pages.empty(); pages.pop()) {
        QImage image = pages.front();
        if (printHeader) {
            painter.drawImage(headerRect, headerImg, headerRect);
        }
        if (printFooter) {
            painter.drawImage(footerRect, footerImg, footerSrc);
        }

        painter.drawImage(contentRect, image, contentRect);
        if (pages.size() > 1) {
            printer.newPage(); // only print a new page, if there are more pages
        }
    }

    // Don't forget to end the painter session
    painter.end();

    return 0;
}

int HeaderFooterHandler::calculateHeaderHeight()
{
    if (header.html_file == "") {
        headerHeight = 0;
        return 0;
    }

    // actual calculations
    qreal height = calculateHeight(header);
    if (height == -1) {
        return 1;
    }

    headerHeight = height;
    return 0;
}

int HeaderFooterHandler::calculateFooterHeight()
{
    if (footer.html_file == "") {
        footerHeight = 0;
        return 0;
    }

    // actual calculations
    qreal height = calculateHeight(footer);
    if (height == -1) {
        return 1;
    }

    footerHeight = height;
    return 0;
}

qreal HeaderFooterHandler::calculateHeight(const ConvertedFile& file) const
{
    auto input = file.html_file;
    auto output = file.pdf_file;
    auto inputFile = QFileInfo(input);
    auto outputFile = QFileInfo(output);
    QEventLoop loop;

    if (!inputFile.exists()) {
        QTextStream(stderr) << "Input File " << input << " does not exist." << "\n";
        return -1;
    }

    if (!inputFile.suffix().endsWith("html")) {
        QTextStream(stderr) << "Input File " << input
                            << " cannot be converted. Only .html Files are allowed."
                            << "\n";
        return -1;
    }

    if (outputFile.suffix().isEmpty()) {
        outputFile.setFile(QDir(outputFile.absoluteFilePath()),
            inputFile.baseName() + ".pdf");
    }

    if (!outputFile.dir().exists()) {
        QTextStream(stderr) << "Target Directory " << outputFile.dir().path()
                            << " does not exist." << "\n";
        return -1;
    }

    QWebEngineView view;
    view.setFixedSize(config.page_size.sizePixels(config.dpi));
    view.show();

    qreal result = -1;

    QObject::connect(&view, &QWebEngineView::loadFinished, [&](bool ok) {
        if (!ok) {
            QTextStream(stderr) << "Failed to load file " << input << "\n";
            loop.exit(2);
            return;
        }

        view.page()->runJavaScript("document.body.offsetHeight", [&](const QVariant& v) {
            result = v.toReal();
            loop.quit();
        });
    });

    view.load(QUrl::fromLocalFile(inputFile.absoluteFilePath()));

    int retCode = loop.exec();
    if (retCode != 0) {
        return -1;
    }

    return result;
}

void HeaderFooterHandler::cleanUp() const
{
    auto headerPdf = QFileInfo(header.pdf_file);
    auto footerPdf = QFileInfo(footer.pdf_file);
    auto tmpFolder = QDir("/tmp");

    if (headerPdf.exists()) {
        tmpFolder.remove(headerPdf.fileName());
    }

    if (footerPdf.exists()) {
        tmpFolder.remove(footerPdf.fileName());
    }
}
