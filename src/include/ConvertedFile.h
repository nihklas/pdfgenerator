//
// Created by Niklas Koll on 17.05.24.
//

#ifndef JAPR_CONVERTEDFILE_H
#define JAPR_CONVERTEDFILE_H

#include <QtCore/QString>

typedef struct {
    QString html_file;
    QString pdf_file;
} ConvertedFile;

#endif // JAPR_CONVERTEDFILE_H
