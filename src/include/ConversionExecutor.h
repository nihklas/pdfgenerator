//
// Created by Niklas Koll on 17.05.24.
//

#ifndef JAPR_WORKERPOOL_H
#define JAPR_WORKERPOOL_H

#include <QtCore/QObject>
#include <stack>

#include "ConvertedFile.h"
#include "Converter.h"

class ConversionExecutor : public QObject {
    Q_OBJECT

public:
    void registerWorker(const ConvertedFile& file, Converter& converter, ConverterConfig config);

    void run(Converter& converter);

private slots: void finishedWorker();

private:
    std::stack<std::function<void()>> workerFunctions;
    void work();
};

#endif // JAPR_WORKERPOOL_H
