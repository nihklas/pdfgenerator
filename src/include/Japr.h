//
// Created by Niklas Koll on 17.05.24.
//

#ifndef JAPR_JAPR_H
#define JAPR_JAPR_H

#include "ConvertedFile.h"
#include <QtCore/QString>
#include <utility>

typedef struct {
    QString input;
    QString output;
} CLIArguments;

typedef struct {
    QString orientation;
    QString page_size;
    QString margin_top;
    QString margin_bottom;
    QString margin_right;
    QString margin_left;
    QString dpi;
    QString header;
    QString footer;
    QString header_spacing;
    QString footer_spacing;
} CLIOptions;

class Japr {
public:
    explicit Japr(CLIArguments arguments, CLIOptions options, QString tempFilePrefix)
        : arguments(std::move(arguments))
        , options(std::move(options))
        , tempFilePrefix(std::move(tempFilePrefix))
    {
    }

    int run();

private:
    CLIArguments arguments;
    CLIOptions options;
    QString tempFilePrefix;
    std::vector<ConvertedFile> files;

    bool checkFiles() const;
    void readAllFiles();
};

#endif // JAPR_JAPR_H
