//
// Created by Niklas Koll on 18.05.24.
//

#ifndef JAPR_HEADERFOOTERHANDLER_H
#define JAPR_HEADERFOOTERHANDLER_H

#include <QtCore/QString>
#include <QtGui/QTextCursor>
#include <utility>

#include "Converter.h"

class HeaderFooterHandler {
public:
    explicit HeaderFooterHandler(Converter* converter, ConverterConfig config,
        QString headerLocation, QString footerLocation,
        const QString& prefix = "")
        : converter(converter)
        , config(std::move(config))
        , header({ .html_file = std::move(headerLocation),
              .pdf_file = "/tmp/" + prefix + "_header.pdf" })
        , footer({ .html_file = std::move(footerLocation),
              .pdf_file = "/tmp/" + prefix + "_footer.pdf" }) {};
    // header and footer pdfs are always in the tmp folder

    int calculateHeaderFooterHeight();
    qreal getHeaderHeight() const;
    qreal getFooterHeight() const;
    int printHeaderOnFile(const QString& pdfFile) const;
    void cleanUp() const;

private:
    ConverterConfig config;
    Converter* converter;
    ConvertedFile header;
    ConvertedFile footer;
    qreal headerHeight = -1;
    qreal footerHeight = -1;

    int calculateHeaderHeight();
    int calculateFooterHeight();
    qreal calculateHeight(const ConvertedFile& file) const;
};

#endif // JAPR_HEADERFOOTERHANDLER_H
