//
// Created by Niklas Koll on 17.05.24.
//

#ifndef JAPR_CONVERTER_H
#define JAPR_CONVERTER_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtGui/QPageSize>
#include <QtPrintSupport/QPrinter>
#include <QtWebEngineWidgets/QWebEngineView>

#include "ConvertedFile.h"

typedef struct {
    bool orientation_portrait;
    QPageSize page_size;
    qreal margin_top;
    qreal margin_bottom;
    qreal margin_right;
    qreal margin_left;
    int dpi;
} ConverterConfig;

class Converter : public QObject {
    Q_OBJECT

public:
    int convert(const ConvertedFile& file, const ConverterConfig& config);

    static QPageLayout getLayoutFromConfig(const ConverterConfig& config);

signals:
    void finishedConverting();
};

#endif // JAPR_CONVERTER_H
