//
// Created by Niklas Koll on 25.04.24.
//

#include <QtCore/QCommandLineParser>
#include <QtWidgets/QApplication>

#include "Japr.h"

#include <cstdlib>
#include <ctime>

QString getRandomString(size_t length)
{
    QString randomString = "";
    char randChar;

    for (size_t i = 0; i < length; ++i) {
        randChar = 'a' + std::rand() % 26; // Generate a random character between 'a' and 'z'
        randomString += randChar;
    }

    return randomString;
}

int main(int argc, char* argv[])
{
    std::srand(std::time(nullptr));
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Converts the web page INPUT into the PDF file OUTPUT.");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument(
        "INPUT", "Input file or directory path. If a directory is given, the "
                 "output location also has to be a directory.");
    parser.addPositionalArgument("OUTPUT",
        "Output file or directory path. If a directory "
        "is given, the input file name will be used.");

    parser.addOption({ "orientation", "Page-Orientation (Portrait, Landscape)",
        "ORIENTATION", "Portrait" });
    parser.addOption({ "page-size", "Page-Size (A4, Letter)", "PAGE_SIZE", "A4" });
    parser.addOption(
        { QStringList() << "margin-top" << "T", "Margin Top", "MARGIN_TOP", "0" });
    parser.addOption({ QStringList() << "margin-bottom" << "B", "Margin Bottom",
        "MARGIN_BOTTOM", "0" });
    parser.addOption({ QStringList() << "margin-left" << "L", "Margin Left",
        "MARGIN_LEFT", "0" });
    parser.addOption({ QStringList() << "margin-right" << "R", "Margin Right",
        "MARGIN_RIGHT", "0" });
    parser.addOption({ "dpi", "DPI of the resulting PDF-File", "DPI", "300" });
    parser.addOption(
        { "header", "Path to the html file to use as the header", "HEADER", "" });
    parser.addOption(
        { "footer", "Path to the html file to use as the footer", "FOOTER", "" });
    parser.addOption({ "header-spacing",
        "Extra spacing between the header and the content",
        "HEADER_SPACING", "0" });
    parser.addOption({ "footer-spacing",
        "Extra spacing between the footer and the content",
        "FOOTER_SPACING", "0" });

    parser.process(QCoreApplication::arguments());

    const QStringList requiredArguments = parser.positionalArguments();
    if (requiredArguments.size() != 2) {
        parser.showHelp(0);
    }

    // Todo better Value checking and error on invalid option value
    const QString& input = requiredArguments.at(0);
    const QString& output = requiredArguments.at(1);
    QString orientation = parser.value("orientation");
    QString page_size = parser.value("page-size");
    QString margin_top = parser.value("margin-top");
    QString margin_bottom = parser.value("margin-bottom");
    QString margin_right = parser.value("margin-right");
    QString margin_left = parser.value("margin-left");
    QString dpi = parser.value("dpi");
    QString header = parser.value("header");
    QString footer = parser.value("footer");
    QString header_spacing = parser.value("header-spacing");
    QString footer_spacing = parser.value("footer-spacing");

    CLIArguments arguments { .input = input, .output = output };

    CLIOptions options {
        .orientation = orientation,
        .page_size = page_size,
        .margin_top = margin_top,
        .margin_bottom = margin_bottom,
        .margin_right = margin_right,
        .margin_left = margin_left,
        .dpi = dpi,
        .header = header,
        .footer = footer,
        .header_spacing = header == "" ? "0" : header_spacing,
        .footer_spacing = footer == "" ? "0" : footer_spacing,
    };

    Japr japr(arguments, options, getRandomString(10));
    return japr.run();
}
