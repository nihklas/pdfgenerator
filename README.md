# Japr - Just-Another-PDF-Renderer

Japr is an HTML-To-PDF-Converter. It stems from the lack of up-to-date 
Converters, that do no rely on a complete Chromium for rendering and printing 
(like [Puppeteer](https://github.com/puppeteer/puppeteer/tree/main#readme)).
As [wkhtmltopdf](https://github.com/wkhtmltopdf/wkhtmltopdf) is discontinued and
now archived, Japr was created as a new alternative for an easy-to-use 
Converter, that uses the modern Browser Standards for HTML-Rendering. It uses
the [Qt WebEngine](https://doc.qt.io/qt-6/qtwebengine-overview.html), so the 
rendering is based on the Chromium project, and therefore has the newest Browser
Standard for HTML/CSS as well as even JavaScript. 

## Installation

- Use Nix Flakes
- Use the [Docker image](https://hub.docker.com/r/nihklas/japr).
- Build it from source

### Nix Flake

To get a binary run

```bash
nix build gitlab:nihklas/japr
```

To build the docker image locally run

```bash
nix build gitlab:nihklas/japr#docker
```

### Docker Image

To use the docker image, refer to the Usage part of this README.

### Building from Source

Using Nix is preferred, but if you don't use Nix or Docker, or are on a
different System Architecture, you need to build it yourself. This requires you
to install Qt on your system. Then, you can follow these instructions:

For the best compatibility, use Qt with version `6.8.0`.

1. Clone the Repository
2. `cmake -S . -B build`
3. `cmake --build build`
4. `cmake --install build`

## Usage

### Directly with Nix

```bash
nix run gitlab:nihklas/japr
```

### Native build

After building, you can use Japr like this:

```bash  
japr <INPUT-PATH> <OUTPUT-PATH>  
```  

- `INPUT-PATH` can either be a File, or a Directory. If it's a Directory, `OUTPUT-PATH` also has to be Directory. It
  then converts all Files with a `.html` Extension into corresponding PDF-Files.
- `OUTPUT-PATH` can either be a File, or a Directory. If it's a Directory, the resulting PDF-files will use the
  source-files names, changing the extension from `.html`to `.pdf`.

You can get more Information about the different options with `japr -h`.

### Docker image

You can use the docker image with this command:

```bash
docker run --rm -v <input-folder>:/in -v <output-folder>:/out nihklas/japr
```

You need to provide these two volumes:

- Input folder
- Output folder

The input folder should contain your `.html`-Files to be converted. They will be put into the output folder.

## Support

If you have any issues or ideas, feel free to open up an Issue on GitLab

## Contributing

Any help and Contributions are generally welcome, just follow the standard way of `Fork -> Branch -> Merge Request` of
Open-Source Development.

## License

Japr follows the [LGPL v3](LICENSE) License
